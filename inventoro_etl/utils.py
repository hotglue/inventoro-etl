import os
import pandas as pd
import re
from datetime import datetime
import base64


def clean_dict_items(dict, value=None):
    return {k: v for k, v in dict.items() if v is not None}


def clean_null(input):
    if isinstance(input, list):
        return [clean_null(i) for i in input]
    elif isinstance(input, dict):
        output = {}
        for k, v in input.items():
            if v is not None:
                output[k] = clean_null(v)
        return output
    return input


def clean_payload(item):
    item = clean_dict_items(item)
    output = {}
    for k, v in item.items():
        if isinstance(v, datetime):
            dt_str = v.strftime("%Y-%m-%dT%H:%M:%S%z")
            if len(dt_str) > 20:
                output[k] = f"{dt_str[:-2]}:{dt_str[-2:]}"
            else:
                output[k] = dt_str
        elif isinstance(v, dict):
            output[k] = clean_payload(v)
        else:
            output[k] = v
    return output


def get_row_value(row, map, name):
    if map.get(name) is None:
        return None
    if map[name] not in row.index:
        return None
    value = row[map[name]]
    if pd.isnull(value):
        return None
    return value


def b64_string(input):
    return base64.b64encode(input.encode()).decode("ascii")


def create_endpoint_payload(endpoint, payload, url_encoding=False):
    output = []
    ids = [id[1:-1] for id in re.findall(r'\{.*?\}', endpoint)]
    for item in payload:
        out = {}
        if not url_encoding:
            id_format = {id:item[id] for id in ids}
        else:
            id_format = {id:b64_string(item[id]) for id in ids}
        out["endpoint"] = endpoint.format(**id_format)
        out["payload"] = {k: v for k, v in item.items() if k not in ids}
        output.append(out)
    output = pd.DataFrame(output).groupby("endpoint").apply(lambda x: list(x["payload"]))
    return output.to_dict()


def snapshot_records(stream_data, stream, snapshot_dir, pk="id", converters={}):
    if os.path.isfile(f'{snapshot_dir}/{stream}.snapshot.csv') :
        snapshot = pd.read_csv(f'{snapshot_dir}/{stream}.snapshot.csv', converters=converters.get(stream))
    else:
        snapshot = None

    if stream_data is not None and snapshot is not None:
        stream_data = pd.concat([snapshot, stream_data])
        stream_data = stream_data.drop_duplicates(pk, keep="last")
        stream_data.to_csv(f'{snapshot_dir}/{stream}.snapshot.csv', index=False)
    elif stream_data is not None and snapshot is None:
        stream_data.to_csv(f'{snapshot_dir}/{stream}.snapshot.csv', index=False)
    else:
        stream_data = snapshot
    
    return stream_data
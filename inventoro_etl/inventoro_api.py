import os
import requests
from requests import Response, HTTPError
import simplejson as json
import backoff
from inventoro_etl.utils import clean_payload
from datetime import datetime
import sys


class BackoffWithMaxTimeException(Exception):
    def __init__(self, message: str, response=None) -> None:
        super().__init__(message)
        self.response = response


class BackoffWithMaxTriesException(Exception):
    def __init__(self, message: str, response=None) -> None:
        super().__init__(message)
        self.response = response

class HttpUnauthorizedException(Exception):
    def __init__(self, message: str, response=None) -> None:
        super().__init__(message)
        self.response = response


def _backoff_expo_max_tries() -> int:
    return int(os.getenv('BACKOFF_EXPO_MAX_TRIES', 10))


def _backoff_expo_factor() -> int:
    return int(os.getenv('BACKOFF_EXPO_FACTOR', 3))


def _backoff_constant_max_time() -> int:
    return int(os.getenv('BACKOFF_CONSTANT_MAX_TIME', 3600))


def _on_request_giveup(details: dict):
    """
    Used to record failed requests after retries has been made
    """
    api: InventoroAPI = details.get('args', [])[0]
    method = details.get('args', [])[1]
    request = details.get('kwargs', {})
    tries = details.get('tries', 0)
    exception: HTTPError = sys.exc_info()[1]  # type: ignore
    response: Response = exception.response

    api._record_stats(method, request, response, tries)


class InventoroAPI:
    def __init__(self, client=None, secret=None, prod=False, chunk_size=50, debug=False, config=None, url_encoding=False, base_url=None):
        if config:
            flow_id = os.environ.get("FLOW", "aaaaaaaaa")
            creds = [x for x in config['flows'] if x['flowId'] == flow_id][0]['apiCredentials']
            self.client = creds["clientId"]
            self.secret = creds["secret"]
        else:
            self.client = client
            self.secret = secret

        env_id = os.environ.get("ENV_ID", "dev.hotglue.inventoro.com")
        if ("prod" in env_id) or prod:
            self.base_url = "https://api.inventoro.com"
        elif ("local" in env_id):
            self.base_url = os.environ.get("INVENTORO_API_BASE_URL", "localhost:8080")
        else:
            self.base_url = "https://api.inventoro.dev"

        if base_url is not None:
            self.base_url = base_url

        self.login_url = os.getenv('INVENTORO_API_LOGIN_URL', '/v1/import/login')

        print(f"[API] base_url={self.base_url}, login_url={self.login_url}")
        print(f"[API] Using backoff (max_tries={_backoff_expo_max_tries()}, factor={_backoff_expo_factor()})")
        print(f"[API] Using backoff (max_time={_backoff_constant_max_time()})")

        self.stats = {}
        self.url_encoding = url_encoding
        self.headers = None
        self._gen_header()
        self.chunk_size = chunk_size
        self.debug = debug

        # Detect region of the customer and override base url if necessary
        res = self.get_dict_payload('/v1/customer')
        is_us_customer = res is not None and res.get('dataStorageLocation', 'EU') == 'US'
        if is_us_customer and base_url is None and "local" not in env_id:
            if ("prod" in env_id) or prod:
                self.base_url = "https://api.us.inventoro.com"
            else:
                self.base_url = "https://api.us.inventoro.dev"

            # Refresh token
            self._gen_header()

        print(f"[API] base_url after region check={self.base_url}")

    def _clean_response_text(self, text: str) -> str:
        return text.replace('error', 'failure').replace('Error', 'Failure')

    @backoff.on_exception(backoff.expo, BackoffWithMaxTriesException, max_tries=_backoff_expo_max_tries, factor=_backoff_expo_factor, on_giveup=_on_request_giveup)
    @backoff.on_exception(backoff.expo, BackoffWithMaxTimeException, max_time=_backoff_constant_max_time, on_giveup=_on_request_giveup)
    @backoff.on_exception(backoff.constant, HttpUnauthorizedException, interval=1, max_tries=_backoff_expo_max_tries, on_giveup=_on_request_giveup)
    def _request_with_retry(self, method: str, retry_login: bool = True, **kwargs) -> Response:

        url = kwargs.get('url')

        # Use current headers
        if self.headers is not None:
            kwargs['headers'] = self.headers

        # Make request
        response = requests.request(method, **kwargs)

        # Response is OK = save stats and return response
        if response.status_code >= 200 and response.status_code < 300:
            self._record_stats(method, kwargs, response, 1)
            return response

        error_msg = f"[API] {method} request failed (url={url}, status_code={response.status_code}, response={self._clean_response_text(response.text)})"

        # Token is expired, refresh
        if response.status_code == 401:
            if retry_login:
                self._gen_header()
            raise HttpUnauthorizedException(error_msg, response=response)

        # Some status codes should not retry
        if response.status_code in [400, 404, 409]:
            self._record_stats(method, kwargs, response, 1)
            raise Exception(error_msg)

        # Custom backoff on HTTP 423
        if response.status_code == 423:
            raise BackoffWithMaxTimeException(error_msg, response=response)

        # Raise HTTPError if response is not OK
        try:
            response.raise_for_status()
        except HTTPError:
            raise BackoffWithMaxTriesException(error_msg, response=response)

        return response

    def _record_stats(self, method: str, request: dict, response: Response, tries: int) -> None:
        """
        Records request statistics
        """
        key = f"{method} {request.get('url')}"

        if len(self.stats.keys()) == 0:
            self.stats['total_success'] = 0
            self.stats['total_fails'] = 0

        if key not in self.stats.keys():
            self.stats[key] = {
                'stats': {
                    'success': 0,
                    'fails': 0,
                },
            }

        if response.status_code >= 200 and response.status_code < 300:
            self.stats[key]['stats']['success'] += 1
            self.stats['total_success'] += 1
        else:
            if 'failed_requests' not in self.stats[key].keys():
                self.stats[key]['failed_requests'] = []

            self.stats[key]['stats']['fails'] += 1
            self.stats['total_fails'] += 1
            self.stats[key]['failed_requests'].append({
                'number': self.stats[key]['stats']['fails'],
                'date': datetime.now(),
                'payload': request.get('data', None),
                'status': response.status_code,
                'response': self._clean_response_text(response.text),
                'tries': tries
            })

    def __del__(self) -> None:
        """
        Destructor method is used to print out requests statistics
        """
        print()
        print("==============================================================")
        print("=============== INVENTORO API STATS ==========================")
        print("==============================================================")
        print()
        print(json.dumps(self.stats, default=str, indent=2))
        print()
        print("==============================================================")
        print("==============================================================")
        print("==============================================================")
        print()
        print()

    def _gen_header(self):
        """
        Peforms a login request to API and saves access_token from response
        """
        endpoint = f"{self.base_url}{self.login_url}"
        credentials = dict(clientId=self.client, secret=self.secret)

        print(f"[API] Login (url={endpoint})")

        response = self._request_with_retry(
            "POST",
            retry_login=False,
            url=endpoint,
            data=json.dumps(credentials)
        )

        print(f"[API] Login success (url={endpoint})")

        access_token = json.loads(response.text).get("access_token")

        self.headers = {
            "Authorization": f"Bearer {access_token}",
            "Content-Type": "application/json",
        }

        if self.url_encoding:
            self.headers["X-Param-Encode"] = "base64url"

    def patch(self, path, payload):
        print(f"[API] PATCH ({path})")

        try:
            return self._request_with_retry(
                "PATCH",
                url=f"{self.base_url}{path}",
                data=json.dumps(payload, default=str, ignore_nan=True)
            )
        except Exception as e:
            print("[API] [ERROR] Patch failed after retries", e)

    def delete(self, path) -> bool:
        print(f"[API] DELETE ({path})")

        try:
            self._request_with_retry(
                "DELETE",
                url=f"{self.base_url}{path}"
            )

            return True
        except Exception as e:
            print("[API] [ERROR] Delete failed after retries", e)
            return False

    def update_summary(self, path, success, fail):
        try:
            # get the name of the stream we're exporting
            key = path[path.rindex("/") + 1:]
            ROOT_DIR = os.environ.get("ROOT_DIR", ".")
            OUTPUT_DIR = f"{ROOT_DIR}/etl-output"
            target_state_path = f'{OUTPUT_DIR}/target_state.json'
            target_state = {"summary": dict()}

            # Read any pre-existing summary file
            if os.path.exists(target_state_path):
                f = open(target_state_path)
                target_state = json.load(f)

            # Get old data
            prev_data = target_state["summary"].get(key, dict())

            # Add the success/fail count for this stream
            target_state["summary"][key] = {
                "success": success + prev_data.get("success", 0),
                "fail": fail + prev_data.get("fail", 0)
            }

            # Write the summary
            with open(target_state_path, "w") as f:
                f.write(json.dumps(target_state))
        except BaseException:
            print("[API] Failed to update summary file!")

    def put_payloads(self, path: str, payloads) -> list:
        print(f"[API] PUT bulk (path={path}, payloads_count={len(payloads)})")

        fails, success, processed_payloads = self.try_put_bulk(path, payloads)
        self.update_summary(path, success, fails)

        print(f"[API] PUT bulk done (path={path}, success={success}, fails={fails})")

        if fails > 0:
            print(f"[API] [ERROR] PUT bulk failed after retries (path={path}, success={success}, fails={fails})")

        return processed_payloads

    def put_dict_payloads(self, payloads) -> list:
        fails = 0
        success = 0
        processed_payloads = []
        payload_total_size = 0

        print(f"[API] PUT bulk by path (chunks_count={len(payloads)})")

        for path, path_payloads in payloads.items():
            print(f"[API] PUT bulk by path (path={path}, path_payloads_count={len(path_payloads)})")

            _fails, _success, _processed_payloads = self.try_put_bulk(path, path_payloads)
            fails += _fails
            success += _success
            processed_payloads.extend(_processed_payloads)
            payload_total_size += len(path_payloads)

            print(f"[API] PUT bulk by path done (path={path}, success={_success}, fails={_fails})")

        if payload_total_size > 0:
            self.update_summary(next(iter(payloads)), success, fails)

        if fails > 0:
            print(f"[API] [ERROR] PUT bulk by path failed after retries (total_payloads_count={payload_total_size}, success={success}, fails={fails})")
        else:
            print(f"[API] PUT bulk by path done all (total_payloads_count={payload_total_size}, success={success}, fails={fails})")

        return processed_payloads

    def try_put_bulk(self, path, payloads):
        endpoint = f"{self.base_url}{path}"

        success = 0
        fails = 0
        processed_payloads = []
        total_size = len(payloads)
        last_check = 0

        for c in range(0, len(payloads), self.chunk_size):
            # Get chunk and clean payloads
            chunk = payloads[c: c + self.chunk_size]
            chunk = [clean_payload(payload) for payload in chunk]

            # Send request with retries - if it fails even after
            # retries, we try to unpack chunk and send payloads
            # one by one
            try:
                self._request_with_retry(
                    "PUT",
                    url=endpoint,
                    data=json.dumps(chunk, default=str, ignore_nan=True)
                )

                success += len(chunk)
                processed_payloads.extend(chunk)
            except Exception:
                print(f"[API] PUT bulk chunk failed, unpacking and trying one by one (path={path}, payloads_count={len(chunk)})")

                for unpacked_payload in chunk:

                    # Here we try to send single payload with retries, if it fails
                    # even after retries we just increment fails count
                    # and proceed with next payload
                    try:
                        self._request_with_retry(
                            "PUT",
                            url=endpoint,
                            data=json.dumps([unpacked_payload], default=str, ignore_nan=True)
                        )

                        success = success + 1
                        processed_payloads.append(unpacked_payload)
                    except Exception as e:
                        print(f"[API] [ERROR] PUT bulk one by one failed after retries (e={e})")
                        fails += 1

            # Notify about overall progress
            if success + fails > last_check + 500:
                progress_perc = round((success + fails) / (total_size / 100), 1)
                print(f"[API] PUT bulk chunk progress (path={path}, success={success}, fails={fails}, progress={progress_perc}%)")
                last_check = success + fails

        return fails, success, processed_payloads

    def put_payloads_by_path(self, path, payloads):
        endpoint = f"{self.base_url}{path}"

        print(f"[API] PUT payloads by path (path={path}, payloads_by_path_count={len(payloads)})")

        for endpoint, item in payloads.items():
            try:
                self._request_with_retry(
                    "PUT",
                    url=endpoint,
                    data=json.dumps(item, default=str, ignore_nan=True)
                )
            except Exception as e:
                print("[API] [ERROR] Put payloads by path failed after retries", e)

    def set_chunk_size(self, chunk_size: int) -> None:
        self.chunk_size = chunk_size

    def post(self, path, payload):
        endpoint = f"{self.base_url}{path}"

        print(f"[API] POST (path={path})")

        try:
            response = self._request_with_retry(
                "POST",
                url=endpoint,
                data=json.dumps(payload, default=str, ignore_nan=True)
            )

            return response.json()
        except Exception as e:
            print("[API] [ERROR] Post failed after retries", e)

    def post_dict_payload(self, path, payload):
        print(f"[API] POST dict payload (path={path})")

        response_content = self.post(path, payload)

        if response_content is None:
            self.update_summary(path, 0, 1)
            return None

        self.update_summary(path, 1, 0)
        return response_content

    def get(self, path):
        endpoint = f"{self.base_url}{path}"

        print(f"[API] GET (path={path})")

        try:
            response = self._request_with_retry(
                "GET",
                url=endpoint
            )

            return response.json()
        except Exception as e:
            print("[API] [ERROR] Get failed after retries", e)
            return None

    def get_dict_payload(self, path):
        print(f"[API] GET dict payload (path={path})")

        response_content = self.get(path)

        if response_content is None:
            self.update_summary(path, 0, 1)
            return None

        self.update_summary(path, 1, 0)
        return response_content

from datetime import datetime
from locale import currency
from typing import List, Optional
from typing_extensions import Literal
from pydantic import BaseModel, Field, validator, constr


class Currency(BaseModel):
    id: constr(max_length=3)
    default: bool = True


class Category(BaseModel):
    id: Optional[constr(curtail_length=50, strip_whitespace=True)] = "not-assigned"
    name: Optional[constr(curtail_length=64, strip_whitespace=True)] = "not-assigned"


class Supplier(BaseModel):
    id: Optional[constr(curtail_length=50, strip_whitespace=True)] = "not-assigned"
    name: Optional[constr(curtail_length=64, strip_whitespace=True)] = "not-assigned"
    currency: Currency


class Warehouse(BaseModel):
    id: constr(curtail_length=50, strip_whitespace=True)
    name: constr(curtail_length=64, strip_whitespace=True)
    parentWarehouseId: Optional[str]


class Properties(BaseModel):
    productIdBySupplier: Optional[constr(curtail_length=100, strip_whitespace=True)]
    ean: Optional[constr(curtail_length=255)]
    collection: Optional[constr(curtail_length=255)]
    brand: Optional[constr(curtail_length=255)]


class Product(BaseModel):
    id: constr(curtail_length=100, strip_whitespace=True)
    name: constr(curtail_length=255)
    shortcut: Optional[constr(curtail_length=255)]
    category: Category
    properties: Optional[Properties]
    shortDescription: Optional[constr(curtail_length=255)]
    description: Optional[constr(curtail_length=255)]
    metaDescription: Optional[constr(curtail_length=255)]


class Image(BaseModel):
    id: constr(curtail_length=255, strip_whitespace=True)
    name: constr(curtail_length=255, strip_whitespace=True)
    url: Optional[constr(max_length=2048, strip_whitespace=True)]
    primary: Optional[bool] = False


class WarehouseProduct(BaseModel):
    warehouse: Warehouse
    product: Product
    availableSupply: int
    stockPrice: Optional[float] = 0
    salePrice: Optional[float] = 0
    supplier: Optional[Supplier]
    minimalOrderQuantity: Optional[float]
    minimalOrderSet: Optional[float]
    order: Optional[bool]
    visible: Optional[bool]
    created: Optional[datetime]
    # images: Optional[List[Image]] = None


transaction_types = Literal[
    "CLIENT_TRANSACTION",
    "SUPPLIER_TRANSACTION",
    "REDISTRIBUTION_DISPENSING",
    "REDISTRIBUTION_RECEIPT",
    "RELEASE_TO_PRODUCTION",
    "INCOME_FROM_PRODUCTION",
    "ADD_TO_FORECAST_WITHOUT_SALES",
    "CHANGE_THE_VALUE_OF_THE_WAREHOUSE_WITHOUT_CHANGING_THE_QUANTITY",
    "OTHER_INCOME",
    "OTHER_DISPENSING",
]


class Transaction(BaseModel):
    warehouseId: constr(curtail_length=64, strip_whitespace=True)
    productId: constr(curtail_length=100, strip_whitespace=True)
    id: constr(curtail_length=100, strip_whitespace=True)
    transactionTypeId: transaction_types
    dateOfTransaction: datetime
    amount: float
    price: float
    stockPrice: Optional[float]
    promoSale: bool = False
    extremeSale: bool = False
    dateOfOrder: datetime
    supplier: Optional[Supplier]


future_delivery_types = Literal[
    "SUPPLIER_ORDER",
    "CLIENT_ORDER",
]


class FutureDelivery(BaseModel):
    warehouseId: constr(curtail_length=64, strip_whitespace=True)
    productId: constr(curtail_length=100, strip_whitespace=True)
    id: constr(curtail_length=100, strip_whitespace=True)
    supplier: Optional[Supplier]
    parentWarehouse: Optional[Warehouse]
    dateOfOrder: datetime
    estimatedDateOfDelivery: datetime
    amount: float
    purchasePrice: Optional[float]
    futureDeliveryType: Optional[future_delivery_types]


promotion_types = Literal[
    "COUPON",
    "GIFT",
    "DISCOUNT",
    "OTHER",
    "FREE_DELIVERY",
    "ADVERTISEMENT",
    "LISTING",
    "DELISTING",
]


class Promotion(BaseModel):
    id: constr(curtail_length=100, strip_whitespace=True)
    type: promotion_types
    productId: constr(curtail_length=100, strip_whitespace=True)
    warehouseId: Optional[str]
    dateFrom: datetime
    dateTo: datetime
    power: float
    baseProductPrice: Optional[float]
    estimatedAmount: float


class Bundle(BaseModel):
    productId: constr(curtail_length=100, strip_whitespace=True)


class BundleProduct(BaseModel):
    productId: constr(curtail_length=100, strip_whitespace=True)
    amount: float

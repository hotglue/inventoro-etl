from locale import currency
from currency_converter import CurrencyConverter
from inventoro_etl.utils import get_row_value, clean_dict_items, clean_null
from inventoro_etl.models import Warehouse, Category, Currency, Properties, Supplier, Product, Transaction, WarehouseProduct, FutureDelivery, Promotion, Image, Bundle, BundleProduct


def price_conversions(stock_price, sale_price, currency_dict, default_currency=None):

    if not (stock_price and sale_price):
        return 0, 0
    
    if default_currency:
        cc = CurrencyConverter()
        stock_price = round(cc.convert(stock_price, currency_dict["id"], default_currency), 2)
        sale_price = round(cc.convert(sale_price, currency_dict["id"], default_currency), 2)

    return stock_price, sale_price


def warehouse_product_payload(df, map, default_currency=None):
    for i, row in df.iterrows():
        warehouse_dict = {
            "id": get_row_value(row, map, "warehouse_id"),
            "name": get_row_value(row, map, "warehouse_name"),
            "parentWarehouseId": get_row_value(row, map, "parent_warehouse_id"),
        }
        warehouse_dict = clean_dict_items(warehouse_dict)
        warehouse = Warehouse(**warehouse_dict)

        category_dict = {
            "id": get_row_value(row, map, "category_id"),
            "name": get_row_value(row, map, "category_name"),
        }
        category_dict = clean_dict_items(category_dict)
        category = Category(**category_dict)

        productIdBySupplier = get_row_value(row, map, "product_id_by_supplier")
        ean = get_row_value(row, map, "ean")
        ean = None if ean=="" else ean
        collection = get_row_value(row, map, "collection")
        brand = get_row_value(row, map, "brand")
        if productIdBySupplier or ean or collection or brand:
            properties_dict = {
                "productIdBySupplier": productIdBySupplier,
                "ean": ean,
                "collection": collection,
                "brand": brand,
            }
            properties_dict = clean_dict_items(properties_dict)
            properties = Properties(**properties_dict)
        else:
            properties = None

        if get_row_value(row, map, "supplier_id") and get_row_value(row, map, "supplier_name") and get_row_value(row, map, "currency"):
            currency_dict = {
                "id": get_row_value(row, map, "currency"),
                "default": get_row_value(row, map, "default_currency"),
            }
            currency_dict = clean_dict_items(currency_dict)
            currency = Currency(**currency_dict)

            supplier_dict = {
                "id": get_row_value(row, map, "supplier_id"),
                "name": get_row_value(row, map, "supplier_name"),
                "currency": currency
            }
            supplier_dict = clean_dict_items(supplier_dict)
            supplier = Supplier(**supplier_dict)
        else:
            supplier = None
        
        product_dict = {
            "id": get_row_value(row, map, "product_id"),
            "name": get_row_value(row, map, "product_name"),
            "shortcut": get_row_value(row, map, "product_shortcut"),
            "category": category,
            "shortDescription": get_row_value(row, map, "short_description"),
            "description": get_row_value(row, map, "description"),
            "metaDescription": get_row_value(row, map, "meta_description")
        }
        if properties is not None:
            product_dict["properties"] = properties

        product = Product(**product_dict)

        stock_price = get_row_value(row, map, "stock_price")
        sale_price = get_row_value(row, map, "sale_price")

        warehouse_product_dict = {
            "warehouse": warehouse,
            "product": product,
            "availableSupply": get_row_value(row, map, "available_supply"),
            "stockPrice": stock_price,
            "salePrice": sale_price,
            "supplier": supplier,
            "minimalOrderQuantity": get_row_value(row, map, "minimal_order_quantity"),
            "minimalOrderSet": get_row_value(row, map, "minimal_order_set"),
            "order": get_row_value(row, map, "is_order"),
            "visible": get_row_value(row, map, "is_visible"),
            "created": get_row_value(row, map, "created"),
        }

        if "images" in map.keys():
            images = []
            for image in get_row_value(row, map, "images"):
                image_dict = clean_dict_items(image)
                images.append(Image(**image_dict))
                warehouse_product_dict["images"] = images

        warehouse_product_dict = clean_dict_items(warehouse_product_dict)
        warehouse_product = WarehouseProduct(**warehouse_product_dict)
        yield clean_null(warehouse_product.dict())


def transactions_payload(df, map, default_currency=None):
    for i, row in df.iterrows():

        if get_row_value(row, map, "supplier_id") and get_row_value(row, map, "supplier_name") and get_row_value(row, map, "currency"):
            currency_dict = {
                "id": get_row_value(row, map, "currency"),
                "default": get_row_value(row, map, "default_currency"),
            }
            currency_dict = clean_dict_items(currency_dict)
            currency = Currency(**currency_dict)

            supplier_dict = {
                "id": get_row_value(row, map, "supplier_id"),
                "name": get_row_value(row, map, "supplier_name"),
                "currency": currency
            }
            supplier_dict = clean_dict_items(supplier_dict)
            supplier = Supplier(**supplier_dict)
        else:
            supplier = None

        stock_price = get_row_value(row, map, "stock_price")
        sale_price = get_row_value(row, map, "sale_price")
        
        transaction_dict = {
            "warehouseId": get_row_value(row, map, "warehouse_id"),
            "productId": get_row_value(row, map, "product_id"),
            "id": get_row_value(row, map, "transaction_id"),
            "transactionTypeId": get_row_value(row, map, "transaction_type"),
            "dateOfTransaction": get_row_value(row, map, "date_of_transaction"),
            "amount": get_row_value(row, map, "amount"),
            "price": sale_price,
            "stockPrice": stock_price,
            "promoSale": get_row_value(row, map, "promo_sale"),
            "extremeSale": get_row_value(row, map, "extreme_sale"),
            "dateOfOrder": get_row_value(row, map, "date_of_order"),
            "supplier": supplier
        }
        transaction_dict = clean_dict_items(transaction_dict)
        transaction = Transaction(**transaction_dict)
        yield clean_null(transaction.dict())


def future_delivery_payload(df, map, default_currency=None):
    for i, row in df.iterrows():

        if get_row_value(row, map, "supplier_id") and get_row_value(row, map, "supplier_name"):
            currency_dict = {
                "id": get_row_value(row, map, "currency"),
                "default": get_row_value(row, map, "default_currency"),
            }
            currency_dict = clean_dict_items(currency_dict)
            currency = Currency(**currency_dict)

            supplier_dict = {
                "id": get_row_value(row, map, "supplier_id"),
                "name": get_row_value(row, map, "supplier_name"),
                "currency": currency
            }
            supplier_dict = clean_dict_items(supplier_dict)
            supplier = Supplier(**supplier_dict)
        else:
            supplier = None

        warehouse_dict = {
            "id": get_row_value(row, map, "warehouse_id"),
            "name": get_row_value(row, map, "warehouse_name"),
            "parentWarehouseId": get_row_value(row, map, "parent_warehouse_id"),
        }
        warehouse_dict = clean_dict_items(warehouse_dict)
        warehouse = Warehouse(**warehouse_dict)

        future_delivery_type = None
        if get_row_value(row, map, "future_delivery_type"):
            future_delivery_type = get_row_value(row, map, "future_delivery_type")
        
        future_delivery_dict = {
            "warehouseId": get_row_value(row, map, "warehouse_id"),
            "productId": get_row_value(row, map, "product_id"),
            "id": get_row_value(row, map, "id"),
            "supplier": supplier,
            "parentWarehouse": warehouse,
            "dateOfOrder": get_row_value(row, map, "date_of_order"),
            "estimatedDateOfDelivery": get_row_value(row, map, "date_of_delivery"),
            "amount": get_row_value(row, map, "amount"),
            "purchasePrice": get_row_value(row, map, "purchase_price"),
            "futureDeliveryType": future_delivery_type,
        }
        future_delivery_dict = clean_dict_items(future_delivery_dict)
        future_delivery = FutureDelivery(**future_delivery_dict)
        yield clean_null(future_delivery.dict())


def promotion_payload(df, map):
    for i, row in df.iterrows():

        promotion_dict = {
            "id": get_row_value(row, map, "id"),
            "type": get_row_value(row, map, "type"),
            "productId": get_row_value(row, map, "product_id"),
            "warehouseId": get_row_value(row, map, "warehouse_id"),
            "dateFrom": get_row_value(row, map, "date_from"),
            "dateTo": get_row_value(row, map, "date_to"),
            "power": get_row_value(row, map, "power"),
            "baseProductPrice": get_row_value(row, map, "base_product_price"),
            "estimatedAmount": get_row_value(row, map, "estimated_amount")
        }
        promotion_dict = clean_dict_items(promotion_dict)
        promotion = Promotion(**promotion_dict)
        yield clean_null(promotion.dict())


def bundle_payload(df, map):
    for i, row in df.iterrows():

        bundle_dict = {
            "productId": get_row_value(row, map, "product_id"),
        }
        bundle_dict = clean_dict_items(bundle_dict)
        bundle = Bundle(**bundle_dict)
        yield clean_null(bundle.dict())


def bundle_product_payload(df, map):
    for i, row in df.iterrows():

        bundle_product_dict = {
            "productId": get_row_value(row, map, "product_id"),
            "amount": get_row_value(row, map, "amount")
        }
        bundle_product_dict = clean_dict_items(bundle_product_dict)
        bundle_product = BundleProduct(**bundle_product_dict)
        yield clean_null(bundle_product.dict())

#!/usr/bin/env python

from setuptools import setup

setup(
    name="inventoro-etl",
    version="0.1.17",
    description="hotglue etl utilities for inventoro",
    author="hotglue",
    url="https://hotglue.xyz",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    install_requires=[
        "pydantic>=1.9.0",
        "typing_extensions==4.0.0",
        "simplejson>=3.11.1",
        "CurrencyConverter==0.16.6",
        "backoff>=1.6"
    ],
    packages=["inventoro_etl"],
)
